<?php
$form_action = '';
$listId = '40246b7e75';
$langue_fr = '';
$langue_en = '';
	
	
		if( $_POST['subject'] != "" ){
			$form_action = 3;
			echo $form_action;
			exit();
		}

		if( $_POST['nom'] == "" || $_POST['prenom'] == "" || $_POST['courriel'] == "" || $_POST['code_postal'] == "" || $_POST['entendu_parler'] == "" ){
			$form_action = 3;
			echo $form_action;
			exit();
		}
		
		if (!filter_var($_POST['courriel'], FILTER_VALIDATE_EMAIL)) {
			$form_action = 4;
			echo $form_action;
			exit();
		}
		
		
		if( $_POST['entendu_parler'] == "Autres" && $_POST['autre_specifier'] == "" ){
			$form_action = 5;
			echo $form_action;
			exit();
		}



				
			$interets = '';
			if (isset($_POST['interets'])) {
				$optionArray = $_POST['interets'];
			
				for ($i=0; $i<count($optionArray); $i++) {
					$interets .= $optionArray[$i].", ";
					
				}
			}
				
			$grandeur = '';
			if (isset($_POST['grandeur'])) {
				$optionArray = $_POST['grandeur'];
			
				for ($i=0; $i<count($optionArray); $i++) {
					$grandeur .= $optionArray[$i].", ";
					
				}
			}
		
			$autre_specifier='';
			if( $_POST['autre_specifier'] != ""){
				$autre_specifier = ' - Précision :'.$_POST['autre_specifier'];
			}else{
				$autre_specifier = "";
			} 
			
				
				
				
				
				
/*****************************************************	

	MAILCHIMP DATA 

*****************************************************/		
		
	
		
		// --> À changer avec les bonnes infos du MailChimp du client
		
		
	//if( $_POST['infolettre'] == "oui" ){
			
		$infolettre = 'oui';

		if( $_POST['langue'] == "FR" ){
			$langue_fr = true;
			$langue_en = false;
		}else if( $_POST['langue'] == "EN"  ){
			$langue_fr = false;
			$langue_en = true;
		}else{  // FR par défaut
			$langue_fr = true;
			$langue_en = false;
		}
		
		$data       = array(
                    'apikey'        => $apikey,
                    'email_address' => htmlentities($_POST['courriel']),
                    'status'        => 'subscribed',  /* écrire subscribed si on ne veut pas de courriel pour confirmer l'adresse courriel, écrire pending pour que la personne confirme son adresse */
                    'merge_fields'  => array(
											'FNAME' => $_POST['prenom'],
											'LNAME' => $_POST['nom'],
											'EMAIL' => $_POST['courriel'],
											'TELEPHONE' => $_POST['telephone'],	/*	telephone	*/
											'CODEPOSTAL' => $_POST['code_postal'],		/*	code postal	*/
	
	
											'INTERESSE' => $interets,		/* Intéressé par  */
											'GRANDEUR' => $grandeur,		/* Grandeur recherché  */
											'ENTENDU' => $_POST['entendu_parler'].$autre_specifier,		/* Entendu parler  */
											'COMMENTS' => $_POST['commentaires'],		/* Commentaires  */
										
									   ), 
									   
									   	'interests'     => array( 
																'49fa1d0e45' => $langue_fr,   /* FR */
																'ea26b033ee' => $langue_en,   /* EN */
														),															
																 
									    'update_existing'   => true);	
				
				
			
				sendDataToMailChimp($listId, $_POST['courriel'], $data);
					
				
				
		/*}else{
					$infolettre = 'non';

			
		}/* END subscribed to infolettre */
			
/*****************************************************	

	END MAILCHIMP DATA 

*****************************************************/		
				
				
				
				
					
				
				
				
				
				
				
			
if( $_POST['langue'] == "FR" ){
	$subjectmail = "Formulaire | Inscription | Voltige";		
}elseif( $_POST['langue'] == "EN" ){
	$subjectmail = "Form | Register | Voltige";
}
/* END LANGUE */
		
			
			
			$message = '
			<html>
			<head>
			  <title>Voltige</title>
			  <style type="text/css">
			body
			{
				font-size:16px;
				font-family:Arial, Helvetica, sans-serif;
				margin:0;
				padding:5px;
				color:#000000;
			}
			</style>
			</head>
			<body>
			<br>
			  <p><strong>'. $subjectmail .'</strong></p>
			  <br>
			<table align="center" width="100%" border="0" cellspacing="5" cellpadding="5">
			  <tbody>
				<tr>
				  <td width="150">Nom complet :</td>
				  <td style="">'.$_POST['prenom'].' '.$_POST['nom'].'</td>
				</tr>
				<tr>
				  <td width="150">Courriel :</td>
				  <td>'.$_POST['courriel'].'</td>
				</tr>
				<tr>
				  <td width="150">Téléphone :</td>
				  <td>'.$_POST['telephone'].'</td>
				</tr>
				<tr>
				  <td width="150">Code postal : </td>
				  <td>'.$_POST['code_postal'].'</td>
				</tr>
				<tr>
				  <td width="150">Interessé par :</td>
				  <td>'.$interets.'</td>
				</tr>
				<tr>
				  <td width="150">Grandeur recherché :</td>
				  <td>'.$grandeur.'</td>
				</tr>
				<tr>
				  <td width="150">Entendu parler :</td>
				  <td>'.$_POST['entendu_parler'].$autre_specifier.'</td>
				</tr>
				<tr>
				  <td width="150">Questions ou commentaires :</td>
				  <td>'.$_POST['commentaires'].'</td>
				</tr>
				<tr>
				  <td width="150">&nbsp;</td>
				  <td>&nbsp;</td>
				</tr>
				<tr>
				  <td width="150">Langue</td>
				  <td>'.$_POST['langue'].'</td>
				</tr>
				<tr>
				  <td width="150">Date :</td>
				  <td>'.date('Y-m-d').'</td>
				</tr>
			  </tbody>
			</table>  
			</body>
			</html>
			';
			
			




$to = 'info@voltigemtl.ca, julie@rougemarketing.com'; 

$headers[] = 'MIME-Version: 1.0';
$headers[] = 'Content-type: text/html; charset=iso-8859-1';
$headers[] = 'From: Rouge marketing <info@devrouge.com>';

$result = mail($to, utf8_decode($subjectmail), utf8_decode($message), implode("\r\n", $headers));
	
	if( $result == true ){
			$form_action = 1;
			echo $form_action;
			exit;
		}else{
			$form_action = 2;
			echo $form_action;
			exit;
			
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
/*****************************************************	

	MAILCHIMP 

*****************************************************/		


/***********************************/	


function sendDataToMailChimp($listId, $courriel, $data){	


$apikey = "b3bda831bbaa8070a55f40ef48c251e0-us16"; // --> à changer

$endpoint   = "http://us16.api.mailchimp.com/3.0/lists/";  //--> à changer    // find your datacenter in your apikey( xxxxxxxxxxxxxxxxxxxxxxxx-us13 <= this is your datacenter)
$auth       = base64_encode( 'user:'. $apikey );

		
			$json_data = json_encode($data);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $endpoint.$listId.'/members/');
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
														'Authorization: Basic '.$auth));
			curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 10);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
			
			$result = curl_exec($ch);
			
			if ( empty($result) ){
				$form_action = 3;
				echo $form_action;
				exit();
			}
			
			/*echo "<pre>";  // Response form mailchimp
			print_r(json_decode($result,true));*/
			
			
			$json = json_decode($result);
			/*echo '<br />1-'.$json->{'status'};*/
			/*exit();*/
			
			
			
			if( $json->{'status'} == 400){  // DO update if user already in my list
				//DO UPDATE
				//echo '<br />2-'.$json->{'status'};
				//exit();
				$userid = md5($courriel);
					
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $endpoint.$listId.'/members/' . $userid);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
					'Authorization: Basic '. $auth));
				curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_TIMEOUT, 10);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
				$result = curl_exec($ch);
				
				
			  /*	echo "<pre>";  // Response form mailchimp
				print_r(json_decode($result,true));*/
				
				if ( empty($result) ){
					$form_action = 3;
					/*echo $form_action;
					exit();*/
				}
			
			
			
				$json = json_decode($result,true);
				//echo $json->{'status'};
			
			
			}


}

/***************************/


/*****************************************************	

	END MAILCHIMP 

*****************************************************/		
