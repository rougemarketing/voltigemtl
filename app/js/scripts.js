// JavaScript Document

$(document).ready(function(){
	"use strict";





if( window.innerWidth <= 1024 ){ 
 //$('.voile').css({'top': $('#perspective-mobile').position().top + 'px' });  
}




if( window.innerWidth >= 1025 ){ 

	$('#perspective').css({'height':$(window).height()});  	
	
	$('#row1 .col1').css({ 'height' :  $('#row1 .col2').height() });
	$('#row1 .col3').css({ 'height' :  $('#row1 .col2').height() });
	
	$('#img-femme-detente').css({ 'height' :  $(window).height()/2 });
	$('#img-course').css({ 'height' :  $(window).height()/2 });
	

	$('section#row1 .inner .col3 #img-femme-detente .wrap-contenu').css({ 'height' :  $('section#row1 .inner .col3 #img-course').position().top  });
	$('section#row1 .inner .col3 #img-course .wrap-contenu').css({'top':$('section#row1 .inner .col3 #img-femme-detente .wrap-contenu').height()});/**/
	
	

		
	if( window.innerWidth >= 1025 ){
		/* FADE IN IMG */
		
		$('#img-femme-detente, #img-course, #img-femme, #img-verres-de-vin, #img-homme').on('mouseenter', function(){
			$(this).find('.wrap-contenu').fadeIn('slow');
		}).on('mouseleave', function(){
			$(this).find('.wrap-contenu').fadeOut();
		});
	}
	/*
	if( window.innerWidth >= 1025 &&  window.innerWidth <= 1025 ){
		$('.wrap-contenu').css({ 'display' :  'block' });
		$('.wrap-contenu').css({ 'background' :  'rgba(38,38,43, 0.5)' });
			
	}*/
	

}
	

	
	
	
	
	/* POPUP | FORMULAIRE */
	
	$('.reservez').on('click', function(){
		
		$('#popup-formulaire').fadeIn();
		$('body').css("height",$( window ).height());
		$('body').css("overflow","hidden");
		
	});
	
	$('.close-popup').on('click', function(){
		
		$('#popup-formulaire').fadeOut();
		$('body').css("height",'100%');
		$('body').css("overflow","visble");
		
	});
	
	
	
	$("#entendu_parler").change(function(){
	
		 if($(this).val() =="Autres" || $(this).val() =="Other")
		  {
			$('#entendu_parler_autre').fadeIn();
		  }
		  else
		  {
			$('#entendu_parler_autre').fadeOut();
		  }
		  
	});
	
		
	
	
	
/***********************************

	FORMULAIRE INSCRIPTION

***********************************/


		$("#formulaireInscription").bind("submit", function() {
			
			$('#nom, #prenom, #courriel, #code_postal, #entendu_parler').removeClass('erreur');
			
			if ( $("#nom").val().length < 1  ) {
				$('#msg').html("<div class='erreur-msg'>Veuillez inscrire votre nom.</div>");
				$('#nom').addClass('erreur');
				return false;
			}
			
			if ( $("#prenom").val().length < 1 ) {
				$('#msg').html("<div class='erreur-msg'>Veuillez inscrire votre prenom.</div>");
				$('#prenom').addClass('erreur');
				return false;
			}
			
			if ( $("#courriel").val().length < 1 ) {
				$('#msg').html("<div class='erreur-msg'>Veuillez inscrire votre courriel.</div>");
				$('#courriel').addClass('erreur');
				return false;
			}
			
			var email = $("#courriel").val();
			var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		
			if (!filter.test(email)) {
				$('#msg').html("<div class='erreur-msg'>Veuillez inscrire un courriel valide.</div>");
				$('#courriel').addClass('erreur');
				return false;
			 }
			
			if ( $("#code_postal").val().length < 1 ) {
				$('#msg').html("<div class='erreur-msg'>Veuillez inscrire votre code postal.</div>");
				$('#code_postal').addClass('erreur');
				return false;
			}
			
			if ( $("#entendu_parler").val().length < 1 ) {
				$('#msg').html("<div class='erreur-msg'>Veuillez spécifier comment vous avez entendu parler de nous.</div>");
				$('#entendu_parler').addClass('erreur');
				return false;
			}
			
			
				$.ajax({
					type		: "POST",
					cache	: false,
					url		: "ajax-formulaire.php",
					data		: $(this).serializeArray(),
					success: function(data) {
						//$.fancybox(data);
						//alert(data);
						//console.log(data);
						if(data == 1){
							
							$('#msg').html("<div class='msg-succes'><br /><br />Nous vous remercions de votre intérêt pour le projet Voltige. Vous faites maintenant partie de notre liste VIP et serez informé des détails et nouvelles informations concernant le projet en plus de recevoir votre invitation au lancement officiel.</div>");
							/*$('#wrap').hide();*/
							$('.inner-popup').css({'height':'275px'});
							$('#formulaireInscription, .txt-popup').hide();
						}
						
						else if(data == 2){
							$('#msg').html("<div class='erreur-msg'><br />Une erreur s'est produite, veuillez réessayer plus tard.<br /><br /></div>");
							$('#formulaireInscription').hide();
						}
						
						else if(data == 3){
							$('#msg').html("<div class='erreur-msg'><br />Veuillez remplir tous les champs requis.<br /><br /></div>");
						}
						
						else if(data == 4){
							$('#msg').html("<div class='erreur-msg'><br />Veuilez inscrire un courriel valide.<br /><br /></div>");
						}
						else if(data == 5){
							$('#msg').html("<div class='erreur-msg'><br />Veuillez spécifier comment vous avez entendu parler de nous.<br /><br /></div>");
							$('#autre_specifier').addClass('erreur');
						}
							
						return false;
					}
				});
			
				return false;
	});

		
	
	
		
	
	
	

/***********************************

	REGISTER FORM

***********************************/
	
		$("#registerForm").bind("submit", function() {
			
			$('#nom, #prenom, #courriel, #code_postal, #entendu_parler').removeClass('erreur');
			
			if ( $("#nom").val().length < 1  ) {
				$('#msg').html("<div class='erreur-msg'>Please enter your name.</div>");
				$('#nom').addClass('erreur');
				return false;
			}
			
			if ( $("#prenom").val().length < 1 ) {
				$('#msg').html("<div class='erreur-msg'>Please enter your surname.</div>");
				$('#prenom').addClass('erreur');
				return false;
			}
			
			if ( $("#courriel").val().length < 1 ) {
				$('#msg').html("<div class='erreur-msg'>Please enter your email.</div>");
				$('#courriel').addClass('erreur');
				return false;
			}
			
			var email = $("#courriel").val();
			var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		
			if (!filter.test(email)) {
				$('#msg').html("<div class='erreur-msg'>Please enter a valid email.</div>");
				$('#courriel').addClass('erreur');
				return false;
			 }
			
			if ( $("#code_postal").val().length < 1 ) {
				$('#msg').html("<div class='erreur-msg'>Please enter your postal code.</div>");
				$('#code_postal').addClass('erreur');
				return false;
			}

			if ( $("#entendu_parler").val().length < 1 ) {
				$('#msg').html("<div class='erreur-msg'>Please specify how you hear about our project.</div>");
				$('#entendu_parler').addClass('erreur');
				return false;
			}
			
			
			

				$.ajax({
					type		: "POST",
					cache	: false,
					url		: "../ajax-formulaire.php",
					data		: $(this).serializeArray(),
					success: function(data) {
						//$.fancybox(data);
						//alert(data);
						//console.log(data);
						if(data == 1){
							
							$('#msg').html("<div class='msg-succes'><br /><br />Thank you for your interest in the Voltige project. You are now on our VIP list and will be notified of details and new information about the project, in addition to receiving your invitation to the official launch.</div>");
							/*$('#wrap').hide();*/
							$('.inner-popup').css({'height':'275px'});
							
							$('#registerForm, .txt-popup').hide();
						}
						
						else if(data == 2){
							$('#msg').html("<div class='erreur-msg'><br />An error has occured, please try again later.<br /><br /></div>");
							$('#registerForm').hide();
						}
						
						else if(data == 3){
							$('#msg').html("<div class='erreur-msg'><br />Please fill all the fields.<br /><br /></div>");
						}
						
						else if(data == 4){
							$('#msg').html("<div class='erreur-msg'><br />Please enter a valid email.<br /><br /></div>");
						}
						else if(data == 5){
							$('#msg').html("<div class='erreur-msg'><br />Please specify how you hear about our project.<br /><br /></div>");
							$('#autre_specifier').addClass('erreur');
						}
							
						return false;
					}
				});
			
				return false;
	});


	
	
	
});





$( window ).on('resize', function() {
	"use strict";
	
  	$('#perspective').css({'height':$(window).height()});
	
	if( window.innerWidth >= 1025 ){  
	
		$('#row1 .col1').css({ 'height' :  $('#row1 .col2').height() });
		$('#row1 .col3').css({ 'height' :  $('#row1 .col2').height() });
		
		$('#img-femme-detente').css({ 'height' :  $(window).height()/2 });
		$('#img-course').css({ 'height' :  $(window).height()/2 });
		
		$('section#row1 .inner .col3 #img-femme-detente .wrap-contenu').css({ 'height' :  $('section#row1 .inner .col3 #img-course').position().top  });
		$('section#row1 .inner .col3 #img-course .wrap-contenu').css({'top':$('section#row1 .inner .col3 #img-femme-detente .wrap-contenu').height()});/**/
	}
	
	
	if( window.innerWidth >= 1025 ){
		/* FADE IN IMG */
		
		/*$('#img-femme-detente, #img-course, #img-femme, #img-verres-de-vin, #img-homme').on('mouseenter', function(){
			$(this).find('.wrap-contenu').fadeIn('slow');
		}).on('mouseleave', function(){*/
			$('#img-femme-detente, #img-course, #img-femme, #img-verres-de-vin, #img-homme').find('.wrap-contenu').fadeOut();
	/*	});*/
	}
	/*
	if( window.innerWidth >= 1025 &&  window.innerWidth <= 1025 ){
		$('.wrap-contenu').css({ 'display' :  'block' });
		$('.wrap-contenu').css({ 'background' :  'rgba(38,38,43, 0.5)' });
			
	}*/
	
		
});
