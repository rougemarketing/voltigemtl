<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="HandheldFriendly" content="true">
<title>Voltige | Studios and Condos | Montréal</title>
<meta name="description" content="1 to 3-bedroom studios and condos. To purchase or to rent. Launch in fall 2017!" />
<link href="../dist/css/reset.css" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
<link href="../dist/css/styles.min.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>
<body>





<section id="row1">
  <div class="inner">
  
  		<div class="col1">
            
       
        	<div class="inner">
        	
                <a href="../" class="langue">FRANÇAIS</a>
                
                <div id="logo"></div>
  				<div id="perspective-mobile"><div class="voile"></div></div>
              
                <div class="contenu">
                
        	
                    1 to 3-bedroom studios and condos<br />
                    To purchase or to rent<br />
                    <strong>Launch in fall 2017!</strong><br />
                    
                    <a href="#" class="btn reservez">Register</a><br />
                    
                  <!--  Toll free : <a href="tel:18448658443" class="telephone"><strong>1 844 VOLTIGE (865-8443)</strong></a>
                    
                    <ul class="medias-sociaux">
                        <li><a href="#" class="instagram"></a></li>
                        <li><a href="#" class="facebook"></a></li>
                    </ul>-->
                    
               </div>
               
               
            </div>
             <!-- inner -->
        </div>
        <!-- col1 -->

  		<div class="col2">
        	<div id="perspective"><div class="voile"></div></div>
        	<!-- perspective -->
        </div>
        <!-- col2 -->
        
  		<div class="col3">
        	<div id="img-femme-detente">
            
                <div class="wrap-contenu">
                    <div class="contenu">
                    
                    		<h3>An integrated project</h3>
                            <p>Voltige spreads its wings and unveils an integrated Transit-Oriented Development (TOD) project, where modernism meets green spaces to offer you an incomparable environment. Featuring both condos and apartments for rent, commercial and office spaces, Voltige has 5 separate phases. </p>
                    
                    </div>     
                    <!-- contenu -->               
                </div>
            	<!-- wrap-contenu -->

            
            
            </div>

        	<div id="img-course">
            
            
                <div class="wrap-contenu">
                    <div class="contenu">
                    
                    		<h3>A unifying living environment</h3>
                            <p>Fly away to an exceptional everyday life combining the proximity of convenience stores, the dynamic rhythm of playgrounds and sports areas, as well as the tranquility of relaxation areas. In the middle of this vast project, you will find a central park, the true heart of the neighbourhood.</p>
                    
                    </div>     
                    <!-- contenu -->               
               </div>
            	<!-- wrap-contenu -->

                    
            </div>
            <!-- img-course -->
            
        </div>
        <!-- col3 -->
        
  </div>
</section>
<!-- row1 -->





<section id="row2">
  <div class="inner">
  
  		<div class="col1">
        	<div id="img-femme">
            
            	<div class="wrap-contenu">
                    <div class="contenu">
                    
                    		<h3>Modern and elegant architecture </h3>
                            <p>Featuring a refined modern style, Voltige will have windows galore and a private balcony for each resident. To ensure the ultimate in luxury, residents will have access to full concierge services. </p>
                    
                    </div>     
                    <!-- contenu -->               
                </div>
            	<!-- wrap-contenu -->
                
            </div>
            <!-- img-femme -->
        </div>
		<!-- col1 -->
  
  		<div class="col2">
        	<div id="img-verres-de-vin">
            	
                <div class="wrap-contenu">
                    <div class="contenu">
                    
                    		<h3>PANORA AND L’ENVOL:<br /> THE FIRST TWO PHASES </h3>
                            <p>The Panora tower will offer rental units, while L’Envol will offer condos for purchase. The project will feature many common areas, including an urban chalet with gym, a children’s section and a relaxation area with chairs, fireplace, TV, billiards, ping-pong and table football tables, in addition to a multi-purpose room. On the second-floor rooftop, you will find a communal garden, a pool, an exercise space and an outdoor BBQ area, and you can admire the different viewpoints of Montreal. A business centre with offices and a conference room will also be integrated into the project. </p>
                           
                           <h4>Smart infrastructure favouring mobility</h4>
                            <p>The project will be equipped with underground parking spaces, storage lockers, parking for bicycles, charging stations for electric vehicles and Communauto car sharing services.</p>  

                            <p><strong>Unveiling to come! <br /><a href="#" class="reservez">Register now to stay informed!</a></strong></p>
                    
                    </div>     
                    <!-- contenu -->               
                </div>
            	<!-- wrap-contenu -->
            
            </div>
        </div>
        <!-- col2 -->

  
  		<div class="col3">
        	<div id="img-homme">
            	
                <div class="wrap-contenu">
                    <div class="contenu">
                    
                    		<h3>A prime location</h3>
                            <p>Promoting proximity to public transit and fast access to major roads, Voltige is located in the heart of the Ahuntsic-Cartierville neighbourhood, on the edge of rue Sauvé Ouest, within walking distance of the Ahuntsic train station, and just steps from Marché Central. </p>
                    
                    </div>     
                    <!-- contenu -->               
                </div>
            	<!-- wrap-contenu -->
            
            </div>
            <!-- img-homme -->
        </div>
    	<!-- col3 -->
  </div>
</section>
<!-- row2 -->




<section id="row-mobile">


	<div class="wrap1">
    	<div id="img-femme-detente"></div>
        <div class="contenu">
                    
             <h3>An integrated project </h3>
             <p>Voltige spreads its wings and unveils an integrated Transit-Oriented Development (TOD) project, where modernism meets green spaces to offer you an incomparable environment. Featuring both condos and apartments for rent, commercial and office spaces, Voltige has 5 separate phases.</p>
                    
        </div>     
       <!-- contenu -->           
    </div>
    <!-- wrap1 -->
    

	<div class="wrap2">
    	<div id="img-course"></div>
        <!-- img-course -->
            <div class="contenu">
            
                    <h3>A unifying living environment</h3>
                    <p>Fly away to an exceptional everyday life combining the proximity of convenience stores, the dynamic rhythm of playgrounds and sports areas, as well as the tranquility of relaxation areas. In the middle of this vast project, you will find a central park, the true heart of the neighbourhood.</p>
            
            </div>     
    </div>
    <!-- wrap2 -->
    
    
    <div class="wrap3">
    
        <div id="img-femme"></div><!-- img-femme -->
            <div class="contenu">
            
                    <h3>Modern and elegant architecture</h3>
                    <p>Featuring a refined modern style, Voltige will have windows galore and a private balcony for each resident. To ensure the ultimate in luxury, residents will have access to full concierge services. </p>
            
            </div>     
            <!-- contenu -->               
    </div>
    <!-- wrap3 -->
      
      
            
    <div class="wrap4">

        	<div id="img-verres-de-vin"></div>
        
            <div class="contenu">
            
                    <h3>PANORA AND L’ENVOL:<br /> THE FIRST TWO PHASES </h3>
                    <p>The Panora tower will offer rental units, while L’Envol will offer condos for purchase. The project will feature many common areas, including an urban chalet with gym, a children’s section and a relaxation area with chairs, fireplace, TV, billiards, ping-pong and table football tables, in addition to a multi-purpose room. On the second-floor rooftop, you will find a communal garden, a pool, an exercise space and an outdoor BBQ area, and you can admire the different viewpoints of Montreal. A business centre with offices and a conference room will also be integrated into the project. </p>
                           
                    <h4>Smart infrastructure favouring mobility</h4> 
                    <p>The project will be equipped with underground parking spaces, storage lockers, parking for bicycles, charging stations for electric vehicles and Communauto car sharing services.</p>  
                    
                    <p><strong>Unveiling to come! <br />
<a href="#" class="reservez">Register now to stay informed!</a></strong></p>
            
            </div>     
            <!-- contenu -->               
        
            
            
    </div>
    <!-- wrap4 -->
    
      
            
    <div class="wrap5">
    
        <div id="img-homme"></div>
        <!-- img-homme --> 
                    
      
            <div class="contenu">
            
                    <h3>A prime location</h3>
                    <p>Promoting proximity to public transit and fast access to major roads, Voltige is located in the heart of the Ahuntsic-Cartierville neighbourhood, on the edge of rue Sauvé Ouest, within walking distance of the Ahuntsic train station, and just steps from Marché Central. </p>
            
            </div>     
            <!-- contenu -->               
       
    </div>
    <!-- wrap5 -->
    
    
                
                   
</section>


<footer id="footer">
  <div class="inner">
  
  		<div class="col1">
        	<div class="innercol">
            
              <a href="#" class="btn reservez">Register</a><br />

                Sales and rental office: <a href="https://www.google.ca/maps/place/1400+Rue+Sauv%C3%A9+O,+Montr%C3%A9al,+QC+H4N+1C5/@45.5337466,-73.6661923,17.91z/data=!4m5!3m4!1s0x4cc918f48bad575d:0x7a526c5746b75cbd!8m2!3d45.5335606!4d-73.6648481" target="_blank">1,400 rue Sauvé Ouest, Montréal QC&nbsp;&nbsp;H4N 1C5</a>
                <br />
                
                <!-- Toll free : <a href="tel:18448658443" class="center">1 844 VOLTIGE (865-8443)</a>-->
            </div>
		</div>
        
        
        <div class="col2">
        <div class="logo-society"></div>
        </div>
  
  
  </div>
</footer>
<!-- footer -->


<div id="popup-formulaire">
	<div class="inner-popup">

		<div class="content-popup">
        	
            <div class="close-popup">X</div>
            
            <div class="logo"></div>
            
           	<p class="txt-popup">Subscribe to our VIP list to receive all project information, news and your invitation to our launch events. </p>
           
           
           	<div id="msg"></div>
            
           	<form id="registerForm" name="registerForm" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
            
            	<input id="nom" name="nom" type="text" value="" placeholder=" Name* :" />
            	<input id="prenom" name="prenom" type="text" value="" placeholder=" Surname* :" />
            	<input id="courriel" name="courriel" type="text" value="" placeholder=" Email* :" />
            	<input id="telephone" name="telephone" type="text" value="" placeholder=" Phone :" />
            	<input id="code_postal" name="code_postal" type="text" value="" placeholder=" Postal code* :" />
            
            
            	<br />
            	<p>I am interested in: <span class="smaller">(you may check more than one response)</span></p>
                <ul class="liste">
                    <li><input type="checkbox" id="interets" name="interets[]" class="chekbox validate[minCheckbox[1]]" value="Rent an apartment" /> Rent an apartment</li>
                    <li><input type="checkbox" id="interets" name="interets[]" class="chekbox validate[minCheckbox[1]]" value="Purchase a condo" /> Purchase a condo</li>
                </ul>
                
                
                <br />
                <p>Required size: <span class="smaller">(you may check more than one response)</span></p>
                <ul class="liste">
                    <li><input type="checkbox" id="grandeur" name="grandeur[]" class="chekbox validate[minCheckbox[1]]" value="Studio" /> Studio</li>
                    <li><input type="checkbox" id="grandeur" name="grandeur[]" class="chekbox validate[minCheckbox[1]]" value="1-bedroom" /> 1-bedroom</li>
                    <li><input type="checkbox" id="grandeur" name="grandeur[]" class="chekbox validate[minCheckbox[1]]" value="2-bedroom" /> 2-bedroom</li>
                    <li><input type="checkbox" id="grandeur" name="grandeur[]" class="chekbox validate[minCheckbox[1]]" value="3-bedroom" /> 3-bedroom</li>
                </ul>

				<br />
                <p>How did you hear about the project?*</p>
                <select id="entendu_parler" name="entendu_parler">
               		<option value="">--- Choose ---</option>
               		<option value="Google search">Google search</option>
               		<option value="Billboard">Billboard</option>
               		<option value="Advertising">Advertising</option>
               		<option value="Social media">Social media</option>
               		<option value="Mail flyer">Mail flyer</option>
               		<option value="Website">Website</option>
               		<option value="Real estate agent or broker">Real estate agent or broker</option>
               		<option value="Other">Other (please specify)</option>
                </select>
                
                <div id="entendu_parler_autre">
               		 <input id="autre_specifier" name="autre_specifier" type="text" value="" placeholder=" Please specify* :" />
                </div>


                <br />
                <p>Questions or comments</p>
                <textarea id="commentaires" name="commentaires" placeholder=" Send us your questions or comments"></textarea>
               
                
                
               	<input id="langue" name="langue" type="text" value="EN" class="fantome" />
               	<input id="subject" name="subject" type="text" value="" class="fantome" />
                <input type="submit" id="btnSubmit" name="btnSubmit" value="SUBMIT" />
            
            </form>
           <br /><br />
           <p>*This field is required.</p>
           

        </div>
		<!-- formulaire -->
	</div>
    <!-- inner-formulaire -->
</div>
<!-- popup-formulaire -->

<script type="text/javascript" src="../dist/js/jquery-1.11.0.min.js"></script> 
<script type="text/javascript" src="../app/js/scripts.js"></script>
</body>
</html>