<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="HandheldFriendly" content="true">
<title>Voltige | Studio et Condos</title>
<meta name="description" content="" />
<link href="dist/css/reset.css" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
<link href="dist/css/styles.min.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>
<body>





<section id="row1">
  <div class="inner">
  
  		<div class="col1">
            
       
        	<div class="inner">
        	
                <a href="en/" class="langue">ENGLISH</a>
                
                <div id="logo"></div>
  				<div id="perspective-mobile"><div class="voile"></div></div>
              
                <div class="contenu">
                
        	
                    Studio et condos 1 à 3 chambres<br />
                    À l'achat ou à la location<br />
                    <strong>Lancement dès l'automne 2017!</strong><br />
                    
                    <a href="#" class="btn reservez">Inscrivez-vous</a><br />
                    
                    <!--Sans frais : <a href="tel:18448658443" class="telephone"><strong>1 844 VOLTIGE (865-8443)</strong></a>
                    
                    <ul class="medias-sociaux">
                        <li><a href="#" class="instagram"></a></li>
                        <li><a href="#" class="facebook"></a></li>
                    </ul>-->
                    
               </div>
               
               
            </div>
             <!-- inner -->
        </div>
        <!-- col1 -->

  		<div class="col2">
        	<div id="perspective"><div class="voile"></div></div>
        	<!-- perspective -->
        </div>
        <!-- col2 -->
        
  		<div class="col3">
        	<div id="img-femme-detente">
            
                <div class="wrap-contenu">
                    <div class="contenu">
                    
                    		<h3>Un projet intégré </h3>
                            <p>Voltige déploie ses ailes et vous dévoile un projet intégré TOD, où la modernité côtoie les espaces verts pour vous offrir un environnement incomparable. Proposant à la fois des condos, des appartements à louer, des espaces commerciaux et des bureaux, Voltige totalise 5 phases distinctes. 
</p>
                    
                    </div>     
                    <!-- contenu -->               
                </div>
            	<!-- wrap-contenu -->

            
            
            </div>

        	<div id="img-course">
            
            
                <div class="wrap-contenu">
                    <div class="contenu">
                    
                    		<h3>Un milieu de vie rassembleur</h3>
                            <p>Envolez-vous vers un quotidien d’exception conjuguant à la fois la praticabilité des commerces de proximité, le rythme dynamique des aires de jeux et de sport ainsi que la tranquillité des espaces de détente. Au centre de ce vaste projet, vous retrouverez un parc central, véritable axe vert du quartier.</p>
                    
                    </div>     
                    <!-- contenu -->               
               </div>
            	<!-- wrap-contenu -->

                    
            </div>
            <!-- img-course -->
            
        </div>
        <!-- col3 -->
        
  </div>
</section>
<!-- row1 -->





<section id="row2">
  <div class="inner">
  
  		<div class="col1">
        	<div id="img-femme">
            
            	<div class="wrap-contenu">
                    <div class="contenu">
                    
                    		<h3>Une architecture moderne et élégante</h3>
                            <p>Présentant un style moderne et épuré, Voltige affichera une fenestration abondante et offrira à chaque résident un balcon privé. Pour assurer un luxe ultime, les résidents auront accès à un service de conciergerie complet.</p>
                    
                    </div>     
                    <!-- contenu -->               
                </div>
            	<!-- wrap-contenu -->
                
            </div>
            <!-- img-femme -->
        </div>
		<!-- col1 -->
  
  		<div class="col2">
        	<div id="img-verres-de-vin">
            	
                <div class="wrap-contenu">
                    <div class="contenu">
                    
                    		<h3>PANORA ET L’ENVOL:<br /> LES 2 PREMIÈRES PHASES</h3>
                            <p>La tour Panora proposera des unités en location alors que l’Envol offrira des condos à l’achat. Le projet sera équipé de nombreuses aires communes, dont un chalet urbain avec gym, une section pour enfants et une aire de détente avec fauteuils, foyer, téléviseur, tables de billard, de ping-pong et de babyfoot en plus d’une salle multifonctionnelle. En toiture-terrasse, vous retrouverez un jardin communautaire, une piscine, un espace d’exercice et une aire pour BBQ extérieurs. Vous pourrez y admirer les différents points de vue de Montréal. Un centre d’affaires avec bureaux et salle de conférence sera intégré au projet.</p>
                            
                            <h4>Des infrastructures intelligentes favorisant la mobilité</h4>
                            <p>Le projet sera pourvu d’espaces de stationnement souterrain, de casiers dédiés à l’entreposage, de stationnement pour les vélos, de bornes de recharge pour véhicules électriques ainsi que les services d’autopartage Communauto.</p>

                            <p><strong>Dévoilement à venir!<br />
<a href="#" class="reservez">Inscrivez-vous pour rester informé!</a></strong></p>
                    
                    </div>     
                    <!-- contenu -->               
                </div>
            	<!-- wrap-contenu -->
            
            </div>
        </div>
        <!-- col2 -->

  
  		<div class="col3">
        	<div id="img-homme">
            	
                <div class="wrap-contenu">
                    <div class="contenu">
                    
                    		<h3>Un emplacement de choix</h3>
                            <p>Favorisant la proximité des transports en commun et un accès rapide aux grands axes routiers, Voltige est situé au cœur du quartier Ahuntsic-Cartierville, en bordure de la rue Sauvé Ouest, à distance de marche de la gare de train Ahuntsic et à deux pas du Marché Central. 
</p>
                    
                    </div>     
                    <!-- contenu -->               
                </div>
            	<!-- wrap-contenu -->
            
            </div>
            <!-- img-homme -->
        </div>
    	<!-- col3 -->
  </div>
</section>
<!-- row2 -->




<section id="row-mobile">


	<div class="wrap1">
    	<div id="img-femme-detente"></div>
        <div class="contenu">
                    
             <h3>Un projet intégré </h3>
             <p>Voltige déploie ses ailes et vous dévoile un projet intégré TOD, où la modernité côtoie les espaces verts pour vous offrir un environnement incomparable. Proposant à la fois des condos, des appartements à louer, des espaces commerciaux et des bureaux, Voltige totalise 5 phases distinctes. </p>
                    
        </div>     
       <!-- contenu -->           
    </div>
    <!-- wrap1 -->
    

	<div class="wrap2">
    	<div id="img-course"></div>
        <!-- img-course -->
            <div class="contenu">
            
                    <h3>Un milieu de vie rassembleur</h3>
                    <p>Envolez-vous vers un quotidien d’exception conjuguant à la fois la praticabilité des commerces de proximité, le rythme dynamique des aires de jeux et de sport ainsi que la tranquillité des espaces de détente. Au centre de ce vaste projet, vous retrouverez un parc central, véritable axe vert du quartier.</p>
            
            </div>     
    </div>
    <!-- wrap2 -->
    
    
    <div class="wrap3">
    
        <div id="img-femme"></div><!-- img-femme -->
            <div class="contenu">
            
                    <h3>Une architecture moderne et élégante</h3>
                    <p>Présentant un style moderne et épuré, Voltige affichera une fenestration abondante et offrira à chaque résident un balcon privé. Pour assurer un luxe ultime, les résidents auront accès à un service de conciergerie complet.</p>
            
            </div>     
            <!-- contenu -->               
    </div>
    <!-- wrap3 -->
      
      
            
    <div class="wrap4">

        	<div id="img-verres-de-vin"></div>
        
            <div class="contenu">
            
                    <h3>PANORA ET L’ENVOL:<br /> LES 2 PREMIÈRES PHASES</h3>
                    <p>La tour Panora proposera des unités en location alors que l’Envol offrira des condos à l’achat. Le projet sera équipé de nombreuses aires communes, dont un chalet urbain avec gym, une section pour enfants et une aire de détente avec fauteuils, foyer, téléviseur, tables de billard, de ping-pong et de babyfoot en plus d’une salle multifonctionnelle. En toiture-terrasse, vous retrouverez un jardin communautaire, une piscine, un espace d’exercice et une aire pour BBQ extérieurs. Vous pourrez y admirer les différents points de vue de Montréal. Un centre d’affaires avec bureaux et salle de conférence sera intégré au projet.</p>
                    <p><strong>Dévoilement à venir! <br />
<a href="#" class="btn reservez">Inscrivez-vous pour rester informé!</a></strong></p>
            
            </div>     
            <!-- contenu -->               
        
            
            
    </div>
    <!-- wrap4 -->
    
      
            
    <div class="wrap5">
    
        <div id="img-homme"></div>
        <!-- img-homme --> 
                    
       
            <div class="contenu">
            
                    <h3>Un emplacement de choix</h3>
                    <p>Favorisant la proximité des transports en commun et un accès rapide aux grands axes routiers, Voltige est situé au cœur du quartier Ahuntsic-Cartierville, en bordure de la rue Sauvé Ouest, à distance de marche de la gare de train Ahuntsic et à deux pas du Marché Central. 
        </p>
            
            </div>     
            <!-- contenu -->               
 
            
    </div>
    <!-- wrap5 -->
    
    
                
                   
</section>


<footer id="footer">
  <div class="inner">
  
  		<div class="col1">
        	<div class="innercol">
            
                <a href="#" class="btn reservez">Inscrivez-vous</a><br />

                Bureau des ventes et locations : <a href="https://www.google.ca/maps/place/1400+Rue+Sauv%C3%A9+O,+Montr%C3%A9al,+QC+H4N+1C5/@45.5337466,-73.6661923,17.91z/data=!4m5!3m4!1s0x4cc918f48bad575d:0x7a526c5746b75cbd!8m2!3d45.5335606!4d-73.6648481" target="_blank">1 400 rue Sauvé Ouest, Montréal  QC  H4N 1C5</a>
                <br />
               <!-- Sans frais : <a href="tel:18448658443" class="center">1 844 VOLTIGE (865-8443)</a>-->
            </div>
		</div>
        
        
        <div class="col2">
        	<div class="logo-society"></div>
        </div>
  
  
  </div>
</footer>
<!-- footer -->


<div id="popup-formulaire">
	<div class="inner-popup">

		<div class="content-popup">
        	
            <div class="close-popup">X</div>
            
            <div class="logo"></div>
            
           	<p class="txt-popup">Inscrivez-vous à notre liste VIP pour recevoir toutes les informations, les nouveautés ainsi que votre invitation à nos évènements de lancement. </p>
           
           
           	<div id="msg"></div>
            
           	<form id="formulaireInscription" name="formulaireInscription" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
            
            	<input id="nom" name="nom" type="text" value="" placeholder=" Nom* :" />
            	<input id="prenom" name="prenom" type="text" value="" placeholder=" Prénom* :" />
            	<input id="courriel" name="courriel" type="text" value="" placeholder=" Courriel* :" />
            	<input id="telephone" name="telephone" type="text" value="" placeholder=" Téléphone :" />
            	<input id="code_postal" name="code_postal" type="text" value="" placeholder=" Code postal* :" />
            
            
            	<br />
            	<p>Je suis intéressé à : <span class="smaller">(vous pouvez cocher plus d’une réponse)</span></p>
                <ul class="liste">
                    <li><input type="checkbox" id="interets" name="interets[]" class="chekbox validate[minCheckbox[1]]" value="Louer un appartement" /> Louer un appartement</li>
                    <li><input type="checkbox" id="interets" name="interets[]" class="chekbox validate[minCheckbox[1]]" value="Acheter un condo" /> Acheter un condo</li>
                </ul>
                
                
                <br />
                <p>Grandeur recherchée : <span class="smaller">(vous pouvez cocher plus d’une réponse)</span></p>
                <ul class="liste">
                    <li><input type="checkbox" id="grandeur" name="grandeur[]" class="chekbox validate[minCheckbox[1]]" value="Studio" /> Studio</li>
                    <li><input type="checkbox" id="grandeur" name="grandeur[]" class="chekbox validate[minCheckbox[1]]" value="1 chambre" /> 1 chambre</li>
                    <li><input type="checkbox" id="grandeur" name="grandeur[]" class="chekbox validate[minCheckbox[1]]" value="2 chambres" /> 2 chambres</li>
                    <li><input type="checkbox" id="grandeur" name="grandeur[]" class="chekbox validate[minCheckbox[1]]" value="3 chambres" /> 3 chambres</li>
                </ul>

				<br />
                <p>Comment avez-vous entendu parler du projet?*</p>
                <select id="entendu_parler" name="entendu_parler">
               		<option value="">--- Choisir ---</option>
               		<option value="Recherche Google">Recherche Google</option>
               		<option value="Enseigne">Enseigne</option>
               		<option value="Publicité">Publicité</option>
               		<option value="Médias sociaux">Médias sociaux</option>
               		<option value="Dépliant postal">Dépliant postal</option>
               		<option value="Site Internet">Site Internet</option>
               		<option value="Courtier ou agent immobilier">Courtier ou agent immobilier</option>
               		<option value="Autres">Autres (spécifier)</option>
                </select>
                
                <div id="entendu_parler_autre">
               		 <input id="autre_specifier" name="autre_specifier" type="text" value="" placeholder=" Spécifier* :" />
                </div>


                <br />
                <p>Questions ou commentaires</p>
                <textarea id="commentaires" name="commentaires" placeholder="Faites-nous part de vos questions ou commentaires"></textarea>
               
                
                
               	<input id="langue" name="langue" type="text" value="FR" class="fantome" />
               	<input id="subject" name="subject" type="text" value="" class="fantome" />
                <input type="submit" id="btnSubmit" name="btnSubmit" value="SOUMETTRE" />
            
            </form>
           
           <br /><br />
           <p>*Ce champ est obligatoire.</p>

        </div>
		<!-- formulaire -->
	</div>
    <!-- inner-formulaire -->
</div>
<!-- popup-formulaire -->

<script type="text/javascript" src="dist/js/jquery-1.11.0.min.js"></script> 
<script type="text/javascript" src="dist/js/scripts.min.js"></script>
</body>
</html>